﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Padevus.Models;

namespace Padevus.Controllers
{
    public class TöötajaController : Controller
    {
        // GET: Töötaja
        public ActionResult Index()
        {
            return View(Töötaja.Töötajad);
        }

        // GET: Töötaja/Details/5
        public ActionResult Details(int id)
        {
            return View(Töötaja.Get(id));
        }

        // GET: Töötaja/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Töötaja/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            //try
            //{
                // TODO: Add insert logic here
                Töötaja töötaja = new Töötaja
                {
                    Id = Töötaja.Töötajad.Max(x => x.Id) + 1,
                    Nimi = collection["Nimi"],
                    Isikukood = collection["Isikukood"],
                    //Oskused.Key = collection["Oskus"],
                    //Oskused.Value = collection["Tase"]

                    //string oskus = collection["Oskused"],
                    //int tase = collection["Tase"],

                    //TODO: kuidas sisestada dictionaryt
                    exel = new Oskused2(collection["exel.Oskus"], collection["exel.Tase"]),

                    //new Dictionary<string, int>
                    //{
                    //    {"programmeerimine", 5 }
                    //},

                    //TODO: kuidas sisestada listi?
                    //Koolitused = collection["Koolitused"]

                    

                    //{ "Scrum koolitus", "Autojuhikoolitus", "Esmaabikoolitus" }
                };

                    Töötaja.Töötajad.Add(töötaja);
               

                return RedirectToAction("Index");
            //}
            //catch (Exception exc)
            //{
            //    Console.WriteLine(exc.Message);
            //    return View();
            //}
        }

        // GET: Töötaja/Edit/5
        public ActionResult Edit(int id)
        {
            Töötaja t = Töötaja.Get(id);
            if (t != null)
            {
                return View(t);
            }
            else
            {
                return RedirectToAction("Index");
            }


        }

        // POST: Töötaja/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                Töötaja t = Töötaja.Get(id);
                if (t != null)
                {
                    t.Nimi = collection["Nimi"];
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Töötaja/Delete/5
        public ActionResult Delete(int id)
        {
            Töötaja t = Töötaja.Get(id);
            if (t != null)
            {
                return View(t);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        // POST: Töötaja/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                Töötaja t = Töötaja.Get(id);
                if (t != null)
                {
                    Töötaja.Töötajad.Remove(t);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
