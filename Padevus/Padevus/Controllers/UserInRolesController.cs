﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Padevus;

namespace Padevus.Controllers
{
    public class UserInRolesController : Controller
    {
        private MartinEntities db = new MartinEntities();

        // GET: UserInRoles
        public ActionResult Index()
        {
            var userInRoles = db.UserInRoles.Include(u => u.Person).Include(u => u.Role);
            return View(userInRoles.ToList());
        }

        // GET: UserInRoles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInRole userInRole = db.UserInRoles.Find(id);
            if (userInRole == null)
            {
                return HttpNotFound();
            }
            return View(userInRole);
        }

        // GET: UserInRoles/Create
        public ActionResult Create()
        {
            ViewBag.PersonId = new SelectList(db.People, "Id", "PersonID");
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "RoleName");
            return View();
        }

        // POST: UserInRoles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,PersonId,RoleId")] UserInRole userInRole)
        {
            if (ModelState.IsValid)
            {
                db.UserInRoles.Add(userInRole);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PersonId = new SelectList(db.People, "Id", "PersonID", userInRole.PersonId);
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "RoleName", userInRole.RoleId);
            return View(userInRole);
        }

        // GET: UserInRoles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInRole userInRole = db.UserInRoles.Find(id);
            if (userInRole == null)
            {
                return HttpNotFound();
            }
            ViewBag.PersonId = new SelectList(db.People, "Id", "PersonID", userInRole.PersonId);
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "RoleName", userInRole.RoleId);
            return View(userInRole);
        }

        // POST: UserInRoles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,PersonId,RoleId")] UserInRole userInRole)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userInRole).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PersonId = new SelectList(db.People, "Id", "PersonID", userInRole.PersonId);
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "RoleName", userInRole.RoleId);
            return View(userInRole);
        }

        // GET: UserInRoles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInRole userInRole = db.UserInRoles.Find(id);
            if (userInRole == null)
            {
                return HttpNotFound();
            }
            return View(userInRole);
        }

        // POST: UserInRoles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserInRole userInRole = db.UserInRoles.Find(id);
            db.UserInRoles.Remove(userInRole);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
