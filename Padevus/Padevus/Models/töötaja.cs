﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Padevus.Models
{
    public class Töötaja
    {
        public Oskused2 exel { get; set; }
        public int Id { get; set; }
        public string Nimi { get; set; }
        public string Isikukood { get; set; }
        //public Dictionary<string, int> Oskused;
        //public string Oskused { get; set; }
        public List<string> Koolitused { get; set; }

        private static List<Töötaja> töötajad = new List<Töötaja>

        {
            new Töötaja {Id = 1, Nimi = "Mauno Saunamees", Isikukood = "36203093333",
                exel= new Oskused2("appi", "5"),
                //Oskused = "Ei ole",
                //new Dictionary<string, int>
                //{
                //    { "Programmeerimine", 5 },
                //    {"Excel", 3 }
                //},
                Koolitused = new List<string> {"Notepad", "wordpad" }},
            new Töötaja
            { Id = 2, Nimi = "Rauno Maunamees", Isikukood = "37004103333",
               // Oskused = null,
               exel= new Oskused2("aeae","6"),
                Koolitused = new List<string> {"Notepad", "wordpad" }
            },
            new Töötaja {Id = 3, Nimi = "Tauno Launamees", Isikukood = "32001013333",
                //Oskused = null,
                Koolitused = new List<string> {"Notepad", "wordpad", "Laisklemine" }}
        };

        public static Töötaja Get(int id) => Töötajad.Where(x => x.Id == id).SingleOrDefault();


        

        public static List<Töötaja> Töötajad { get => töötajad; set => töötajad = value; }
    }

    public class Oskused2
    {
        public Oskused2(string oskus = "", string tase = "")
        {
            Oskus = oskus;
            Tase = tase;

        }

        public string Oskus { get; set; }
        public string Tase { get; set; }

    }
}