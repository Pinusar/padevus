﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WorkersQualification.Startup))]
namespace WorkersQualification
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
