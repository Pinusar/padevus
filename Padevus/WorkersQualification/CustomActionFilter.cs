﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WorkersQualification.Controllers;
using WorkersQualification.Models;
using System.Data;
using System.Data.Entity;
using System.Net;

namespace WorkersQualification

{  //this filter is made for logging actions (saves info using ReportLogsController) 
    public class CustomActionFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var actionName = filterContext.ActionDescriptor.ActionName.ToString();
            if (actionName == "Delete")
            {
                MartinEntities db = new MartinEntities();

                if (filterContext.HttpContext.Session[Constants.SESSION_ACTIVITY_LOG_ID] != null)
                {
                    try
                    {
                        //Controller's name
                        string controllershort = (filterContext.Controller.ToString()).Split('.')[2];

                        // string for logging, contains User name; Controlles name and Action name
                        string repLog = $"{Person.GetByEmail(HttpContext.Current.User.Identity.Name).FullName} " +
                                $"kasutas kontrolleris {controllershort} meetodit {filterContext.ActionDescriptor.ActionName.ToString()}, ";

                        //additional info for logging according to executed controller
                        switch (controllershort)
                        {
                            case "PeopleController":
                                repLog = repLog +
                                $"töötaja: {Person.GetById(int.Parse(filterContext.Controller.ValueProvider.GetValue("id").AttemptedValue)).FullName}";
                                break;
                            case "DepartmentsController":
                                repLog = repLog +
                                $"Osakond: {db.Departments.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("id").AttemptedValue)).DepartmentName} ";
                                break;
                            case "CertificatesController":
                                repLog = repLog +
                                $" Sertifikaat: {db.Certificates.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("id").AttemptedValue)).CertName}";
                                break;
                            case "CompetenciesController":
                                repLog = repLog +
                                $"Oskus: {db.Competencies.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("id").AttemptedValue)).Knowledge.KnowNameEst}, " +
                                $"Töötaja: {db.Competencies.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("id").AttemptedValue)).Person.FullName}";
                                break;
                            case "KnowledgesController":
                                repLog = repLog +
                                $"Oskus: {db.Knowledges.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("id").AttemptedValue)).KnowNameEst}";
                                break;
                            case "RolesController":
                                repLog = repLog +
                                $"Rool: {db.Roles.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("id").AttemptedValue)).RoleName}";
                                break;
                            case "TasksController":
                                repLog = repLog +
                                $"Ülesanne: {db.Tasks.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("id").AttemptedValue)).TaskName}";
                                break;
                            case "TrainingsController":
                                repLog = repLog +
                                $"Koolitus: {db.Trainings.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("id").AttemptedValue)).TrainingName}";
                                break;
                            default:
                                break;
                        }

                        ReportLog log = new ReportLog
                        {
                            UserID = WorkersQualification.Models.Person.GetByEmail(HttpContext.Current.User.Identity.Name).Id,
                            LogDate = DateTime.Now,
                            LogRecord = repLog
                            //filterContext.HttpContext.Session[Constants.SESSION_ACTIVITY_LOG_ID].ToString(),
                            //filterContext.HttpContext.User.Identity.Name,
                            //filterContext.Controller.ToString(),
                            //filterContext.ActionDescriptor.ActionName
                        };
                        var con = DependencyResolver.Current.GetService<ReportLogsController>();
                        con.Create2(log);
                    }
                    catch
                    {
                        // Don't raise any exceptions if the logging fails.
                    }
                }
                base.OnActionExecuting(filterContext);
            }
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var actionName = filterContext.ActionDescriptor.ActionName.ToString();
            if (actionName != "Delete")
            {
                MartinEntities db = new MartinEntities();

                if (filterContext.HttpContext.Session[Constants.SESSION_ACTIVITY_LOG_ID] != null)
                {
                    try
                    {
                        //Controller's name
                        string controllershort = (filterContext.Controller.ToString()).Split('.')[2];

                        // string for logging, contains User name; Controlles name and Action name
                        string repLog = $"{Person.GetByEmail(HttpContext.Current.User.Identity.Name).FullName} " +
                                $"kasutas kontrolleris {controllershort} meetodit {filterContext.ActionDescriptor.ActionName.ToString()}, ";

                        //additional info for logging according to executed controller
                        switch (controllershort)
                        {
                            case "PeopleController":
                                if (actionName == "AddRole")
                                {
                                    repLog = repLog +
                                    $"Töötajale {db.People.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("id").AttemptedValue)).FullName}" +
                                    $" lisatud Rool: {db.Roles.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("roleid").AttemptedValue)).RoleName}";
                                    break;
                                }
                                if (actionName == "RemoveRole")
                                {
                                    repLog = repLog +
                                    $"Töötajalt {db.People.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("id").AttemptedValue)).FullName}" +
                                    $" kustutatud Rool: {db.Roles.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("roleid").AttemptedValue)).RoleName}";
                                    break;
                                }
                                if (actionName == "TaskRemove")
                                {
                                    repLog = repLog +
                                    $"Töötajalt {db.People.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("id").AttemptedValue)).FullName}" +
                                    $" kustutatud Ülesanne: {db.Tasks.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("taskid").AttemptedValue)).TaskName}";
                                    break;
                                }
                                repLog = repLog +
                                $"Töötaja: {Person.GetByEmail(filterContext.Controller.ValueProvider.GetValue("Email").AttemptedValue).FullName}";
                                break;
                            case "DepartmentsController":
                                repLog = repLog +
                                $"Osakond: {filterContext.Controller.ValueProvider.GetValue("DepartmentName").AttemptedValue} ";
                                break;
                            case "CertificatesController":
                                repLog = repLog +
                                $" Sertifikaat: {filterContext.Controller.ValueProvider.GetValue("CertName").AttemptedValue}";
                                break;
                            case "CompetenciesController":
                                repLog = repLog +
                                $"Oskus: {db.Knowledges.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("KnowId").AttemptedValue)).KnowNameEst}, " +
                                $"Töötaja: {db.People.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("PersonID").AttemptedValue)).FullName}";
                                break;
                            case "KnowledgesController":
                                repLog = repLog +
                                $"Oskus: {filterContext.Controller.ValueProvider.GetValue("KnowNameEst").AttemptedValue}";
                                break;
                            case "RolesController":
                                repLog = repLog +
                                $"Rool: {filterContext.Controller.ValueProvider.GetValue("RoleName").AttemptedValue}";
                                break;
                            case "TasksController":
                                repLog = repLog +
                                $"Ülesanne: {filterContext.Controller.ValueProvider.GetValue("TaskName").AttemptedValue}, " +
                                $"AssignedId: {db.People.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("AssignedId").AttemptedValue)).FullName}, " +
                                $"AssignerId: {db.People.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("AssignerId").AttemptedValue)).FullName}";
                                break;
                            case "TrainingsController":
                                repLog = repLog +
                                $"Koolitus: {filterContext.Controller.ValueProvider.GetValue("TrainingName").AttemptedValue}, " +
                                $"Töötaja: {db.People.Find(int.Parse(filterContext.Controller.ValueProvider.GetValue("PersonId").AttemptedValue)).FullName}";
                                break;
                            default:
                                break;
                        }

                        ReportLog log = new ReportLog
                        {
                            UserID = WorkersQualification.Models.Person.GetByEmail(HttpContext.Current.User.Identity.Name).Id,
                            LogDate = DateTime.Now,
                            LogRecord = repLog
                        };
                        var con = DependencyResolver.Current.GetService<ReportLogsController>();
                        con.Create2(log);
                    }
                    catch
                    {
                        // Don't raise any exceptions if the logging fails.
                    }
                }
                base.OnActionExecuted(filterContext);
            }
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
        }
    }
}