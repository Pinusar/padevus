﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorkersQualification.Models;

namespace WorkersQualification
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        MartinEntities context = new MartinEntities(); // my entity  
        private readonly string[] allowedroles;
        public CustomAuthorizeAttribute(params string[] roles)
        {
            this.allowedroles = roles;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;
            foreach (var role in allowedroles)
            {
             /* getting user form current context */ 
                if (WorkersQualification.Models.Person
                     .GetByEmail(httpContext.User.Identity.Name)
                     .UserInRoles
                     .Select(x => x.Role.RoleName)
                     .Contains("Admin"))
                {
                    authorize = true; /* return true if Entity has current user(active) with specific role */
                }
            }
            return authorize;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpUnauthorizedResult();
        }
    }
}