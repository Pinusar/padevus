﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorkersQualification.Models;

namespace WorkersQualification.Controllers
{
    public class FilesController : Controller
    {
        private MartinEntities db = new MartinEntities();

        public ActionResult Get(int? id)
        {
            if (!id.HasValue) return HttpNotFound();
            File file = db.Files.Find(id.Value);
            if (file == null) return HttpNotFound();
            return File(file.Content, file.ContentType);
        }

    }
}