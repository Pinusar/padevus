﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WorkersQualification.Models;

namespace WorkersQualification.Controllers
{
    [Authorize]
    public class CertificatesController : Controller
    {
        private MartinEntities db = new MartinEntities();

        // GET: Certificates
        [CustomAuthorize("Admin")]
        public ActionResult Index()
        {
            var certificates = db.Certificates.Include(c => c.File).Include(c => c.Knowledge).Include(c => c.Level).Include(c => c.Person);
            return View(certificates.ToList());
        }

        // GET: Certificates/Details/5
        [CustomAuthorize("Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return View("~/Views/Home/Index.cshtml");
            }
            Certificate certificate = db.Certificates.Find(id);
            if (certificate == null)
            {
                return HttpNotFound();
            }
            return View(certificate);
        }

        // GET: Certificates/Create
        [CustomAuthorize("Admin")]
        public ActionResult Create()
        {
            ViewBag.FileID = new SelectList(db.Files, "Id", "Filename");
            ViewBag.KnowId = new SelectList(db.Knowledges, "Id", "KnowNameEst");
            ViewBag.LevelId = new SelectList(db.Levels, "Id", "LevelNameEst");
            ViewBag.PersonID = new SelectList(db.People, "Id", "FullName");
            return View();
        }

        // POST: Certificates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [CustomActionFilter]
        [ValidateAntiForgeryToken]
        [CustomAuthorize("Admin")]
        public ActionResult Create([Bind(Include = "id,Issuer,Issued,PersonID,CertName,KnowId,LevelId,FileID")] Certificate certificate)
        {
            if (ModelState.IsValid)
            {
                db.Certificates.Add(certificate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FileID = new SelectList(db.Files, "Id", "Filename", certificate.FileID);
            ViewBag.KnowId = new SelectList(db.Knowledges, "Id", "KnowNameEst", certificate.KnowId);
            ViewBag.LevelId = new SelectList(db.Levels, "Id", "LevelNameEst", certificate.LevelId);
            ViewBag.PersonID = new SelectList(db.People, "Id", "FullName", certificate.PersonID);
            return View(certificate);
        }

        // GET: Certificates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return View("~/Views/Home/Index.cshtml");
            }
            Certificate certificate = db.Certificates.Find(id);
            if (certificate == null)
            {
                return HttpNotFound();
            }
            ViewBag.FileID = new SelectList(db.Files, "Id", "Filename", certificate.FileID);
            ViewBag.KnowId = new SelectList(db.Knowledges, "Id", "KnowNameEst", certificate.KnowId);
            ViewBag.LevelId = new SelectList(db.Levels, "Id", "LevelNameEst", certificate.LevelId);
            ViewBag.PersonID = new SelectList(db.People, "Id", "FullName", certificate.PersonID);

            if (!WorkersQualification.Models.Person.GetByEmail(User.Identity.Name).IsAdmin() && (User.Identity.Name != db.Certificates.Where(x=>x.id==id).SingleOrDefault().Person.Email))
            {
                ViewBag.ErrorMessage = "Need Admin rights to see other certificates";
                return View("~/Views/Home/Index.cshtml");
            }
            return View(certificate);
        }

        // POST: Certificates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomActionFilter]
        public ActionResult Edit([Bind(Include = "id,Issuer,Issued,PersonID,CertName,KnowId,LevelId,FileID")] Certificate certificate, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(certificate).State = EntityState.Modified;
                db.Entry(certificate).Property(x => x.FileID).IsModified = false;

                db.SaveChanges();

                if (file != null && file.ContentLength > 0)
                {
                    using (System.IO.BinaryReader br = new System.IO.BinaryReader(file.InputStream))
                    {
                        // ma lihtsuse mõttes kustutan vana pildi baasist
                        File v = db.Files.Find(certificate.FileID ?? 0);

                        byte[] buff = br.ReadBytes(file.ContentLength);
                        File f = new File
                        {
                            Content = buff,
                            ContentType = file.ContentType,
                            Filename = file.FileName.Split('\\').Last().Split('/').Last()
                        };
                        db.Files.Add(f);
                        db.SaveChanges();
                        certificate.FileID = f.Id;
                        db.SaveChanges();
                        if (v != null)
                        {

                            try
                            {
                                db.Files.Remove(v);
                                db.SaveChanges();
                            }
                            catch (Exception e)
                            {

                                
                            }
                        }
                    }

                }

                ViewBag.FileID = new SelectList(db.Files, "Id", "Filename", certificate.FileID);
                ViewBag.KnowId = new SelectList(db.Knowledges, "Id", "KnowNameEst", certificate.KnowId);
                ViewBag.LevelId = new SelectList(db.Levels, "Id", "LevelNameEst", certificate.LevelId);
                ViewBag.PersonID = new SelectList(db.People, "Id", "FullName", certificate.PersonID);

                if (!WorkersQualification.Models.Person.GetByEmail(User.Identity.Name).IsAdmin())
                {
                    return RedirectToAction($"Details/{WorkersQualification.Models.Person.GetByEmail(User.Identity.Name).Id}", "People");
                }
                return RedirectToAction("Index");
            }

                return View(certificate);
        } 

        // GET: Certificates/Delete/5
        public ActionResult Delete(int? id)
            {
                if (id == null)
                {
                    return View("~/Views/Home/Index.cshtml");
                }
                Certificate certificate = db.Certificates.Find(id);
                if (certificate == null)
                {
                    return HttpNotFound();
                }
                return View(certificate);
            } 

        // POST: Certificates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomActionFilter]
        public ActionResult DeleteConfirmed(int id)
        {
            Certificate certificate = db.Certificates.Find(id);
            db.Certificates.Remove(certificate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomActionFilter]
        public ActionResult AddCertificate([Bind(Include = "id")]Certificate certificate0, HttpPostedFile certificatefile)
        {
            int id = certificate0.id;
            Certificate certificate = db.Certificates.Find(id);
            if (certificate == null) return RedirectToAction("Index");
            if (certificatefile == null || certificatefile.ContentLength == 0) return RedirectToAction("Details", new { id = id });
            File newFile = new File();
            using (System.IO.BinaryReader br = new System.IO.BinaryReader(certificatefile.InputStream))
            {
                byte[] buff = br.ReadBytes(certificatefile.ContentLength);
                newFile.Content = buff;
            }
            db.SaveChanges();
            certificate.FileID = newFile.Id;
            db.SaveChanges();


            return RedirectToAction("Details", new { id = id });
        }
    }
}
