﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WorkersQualification.Models;

namespace WorkersQualification.Models
{
    partial class Person
    {
        static MartinEntities db = new MartinEntities();
        public string FullName => FirstName + " " + LastName;

        public static Person GetByEmail(string email)
            => db.People.Where(x => x.Email == email).SingleOrDefault();

        public static Person GetById(int id)
            => db.People.Where(x => x.Id == id).SingleOrDefault();

        public bool IsAdmin() => this.UserInRoles
            .Select(x => x.Role.RoleName)
            .Contains("Admin");
    }
}

namespace WorkersQualification.Controllers
{
    [Authorize]
    public class PeopleController : Controller
    {
        private MartinEntities db = new MartinEntities();

        public static List<Person> allpeople()
        {
            MartinEntities db = new MartinEntities();
            return db.People.ToList();
        }

        public static int completedTasks(Person x)
        {
            return x.Tasks.Where(y => y.Completed != null).Count();
        }

        [CustomActionFilter]
        public ActionResult TaskRemove(int? id, int? taskId)
        {
            Task task = db.Tasks.Find(taskId);
            Person person = db.People.Find(id);
            var actTask = db.Tasks.Where(x => x.Id == taskId).FirstOrDefault();
            if (actTask != null && actTask.Completed == null && actTask.Started != null)
            {
                actTask.Completed = DateTime.Now;
                db.SaveChanges();
            }
            if (actTask != null && actTask.Started == null)
            {
                actTask.Started = DateTime.Now;
                db.SaveChanges();
            }
            return RedirectToAction("Details", new { id = id });
        }

        [CustomActionFilter]
        [CustomAuthorize("Admin")]
        public ActionResult AddRole(int? id, int? roleid)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            if (roleid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Role role = db.Roles.Find(roleid);
            if (role == null)
            {
                return HttpNotFound();
            }
            try
            {
                db.UserInRoles.Add(new UserInRole { PersonId = id.Value, RoleId = roleid.Value });
                db.SaveChanges();
            }
            catch (Exception)
            { }
            return RedirectToAction("Details", new { id = id });
        }

        [CustomActionFilter]
        [CustomAuthorize("Admin")]
        public ActionResult RemoveRole(int? id, int? roleid)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            if (roleid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var userInRole = person.UserInRoles.Where(x => x.RoleId == roleid).FirstOrDefault();
            if (userInRole != null)
            {
                db.UserInRoles.Remove(userInRole);
                db.SaveChanges();
            }
            return RedirectToAction("Details", new { id = id });
        }

        // GET: People
        [CustomAuthorize("Admin")]
        public ViewResult Index(string sortOrder, string searchString = "",
            int page = 0, int size = 10,
            string knowledgesearch = "")
        {
            //var people = db.People.Include(p => p.Department).Include(p => p.File);
            var people = from p in db.People
                         select p;

            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.DepartmentSortParm = String.IsNullOrEmpty(sortOrder) ? "Department" : "";
            ViewBag.Page = page;
            ViewBag.Size = size;
            ViewBag.Pages = people.Count() / size;
            ViewBag.Count = people.Count();


            if (!String.IsNullOrEmpty(searchString))
            {
                people = people.Where(p => p.LastName.Contains(searchString)
                                       || p.FirstName.Contains(searchString)
                                       || p.PersonID.ToString().Contains(searchString)
                                       || p.Department.DepartmentName.Contains(searchString));

            }

            if (!String.IsNullOrEmpty(knowledgesearch))
            {
                people = people.Where(p => p.Competencies
                .Select(c => c.Knowledge.KnowNameEng).Contains(knowledgesearch));

            }

            switch (sortOrder)
            {
                case "name_desc":
                    people = people.OrderBy(p => p.LastName);
                    return View(people
                .Skip(page * size)
                .Take(size)
                .ToList());
                case "Date":
                    people = people.OrderBy(p => p.StartDate);
                    return View(people
                .Skip(page * size)
                .Take(size)
                .ToList());
                case "date_desc":
                    people = people.OrderByDescending(p => p.StartDate);
                    return View(people
                .Skip(page * size)
                .Take(size)
                .ToList());
                case "Department":
                    people = people.OrderBy(p => p.Department.DepartmentName);
                    return View(people
                .Skip(page * size)
                .Take(size)
                .ToList());
                default:
                    break;
            }

            return View(people
                .OrderBy(x => x.Id)
                .Skip(page * size)
                .Take(size)
                .ToList());
        }


        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return View("~/Views/Home/Index.cshtml");
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
                ViewBag.OtherRoles = db.Roles.
                Where(x => !(x.UserInRoles.Select(y => y.PersonId))
                .Contains(id.Value))
                .ToList();

            if (!WorkersQualification.Models.Person.GetByEmail(User.Identity.Name).IsAdmin()&& id != WorkersQualification.Models.Person.GetByEmail(User.Identity.Name).Id)
            {
                ViewBag.ErrorMessage = "Need Admin rights to see other people";
                return View("~/Views/Home/Index.cshtml");
            }   
            return View(person);
        }

        // GET: People/Create
        [CustomAuthorize("Admin")]
        public ActionResult Create()
        {
            ViewBag.DepartmentID = new SelectList(db.Departments, "Id", "DepartmentName");
            ViewBag.Picture = new SelectList(db.Files, "Id", "Filename");
            return View();
        }
        
        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomActionFilter]
        public ActionResult Create([Bind(Include = "Id,PersonID,FirstName,LastName,StartDate,PersonState,DepartmentID,Picture,Email")] Person person)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.People.Add(person);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                    ViewBag.ErrorMessage = "sellise isikukoodi või emailiga vist on juba keegi";

                }

            }

            ViewBag.DepartmentID = new SelectList(db.Departments, "Id", "DepartmentName", person.DepartmentID);
            ViewBag.Picture = new SelectList(db.Files, "Id", "Filename", person.Picture);
            return View(person);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomActionFilter]
        public ActionResult AddPicture([Bind(Include = "Id")]Person person0, HttpPostedFile pilt)
        {
            int id = person0.Id;
            Person person = db.People.Find(id);
            if (person == null) return RedirectToAction("Index");
            if (pilt == null || pilt.ContentLength == 0) return RedirectToAction("Details", new { id = id });
            File newFile = new File();
            using (System.IO.BinaryReader br = new System.IO.BinaryReader(pilt.InputStream))
            {
                byte[] buff = br.ReadBytes(pilt.ContentLength);
                newFile.Content = buff;
            }
            db.SaveChanges();
            person.Picture = newFile.Id;
            db.SaveChanges();

            return RedirectToAction("Details", new { id = id });
        }


        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return View("~/Views/Home/Index.cshtml");
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }

            ViewBag.DepartmentID = new SelectList(db.Departments, "Id", "DepartmentName", person.DepartmentID);

            if (!WorkersQualification.Models.Person.GetByEmail(User.Identity.Name).IsAdmin() && id != WorkersQualification.Models.Person.GetByEmail(User.Identity.Name).Id)
            {
                ViewBag.ErrorMessage = "Need Admin rights to see other people";
                return View("~/Views/Home/Index.cshtml");
            }
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomActionFilter]
        public ActionResult Edit([Bind(Include = "Id,PersonID,FirstName,LastName,StartDate,PersonState,DepartmentID,Email")] Person person, HttpPostedFileBase file)
        {

            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.Entry(person).Property(x => x.Picture).IsModified = false;
                person.Id = db.People.Where(x => x.Id == person.Id).Select(x => x.Id).Single();
                db.SaveChanges();

                if (file != null && file.ContentLength > 0)
                {
                    using (System.IO.BinaryReader br = new System.IO.BinaryReader(file.InputStream))
                    {
                        // ma lihtsuse mõttes kustutan vana pildi baasist
                        File v = db.Files.Find(person.Picture ?? 0);
                        if (v != null)
                        {
                            db.Files.Remove(v);
                            db.SaveChanges();
                        }
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        File f = new File
                        {
                            Content = buff,
                            ContentType = file.ContentType,
                            Filename = file.FileName.Split('\\').Last().Split('/').Last()
                        };
                        db.Files.Add(f);
                        db.SaveChanges();
                        person.Picture = f.Id;
                        db.SaveChanges();
                    }
                }
                if (!WorkersQualification.Models.Person.GetByEmail(User.Identity.Name).IsAdmin())
                {
                    return RedirectToAction($"Details/{WorkersQualification.Models.Person.GetByEmail(User.Identity.Name).Id}");
                }
                return RedirectToAction("Index");
            }
            
            return View(person);
        }

        // GET: People/Delete/5
        [CustomAuthorize("Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return View("~/Views/Home/Index.cshtml");
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize("Admin")]
        [CustomActionFilter]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
