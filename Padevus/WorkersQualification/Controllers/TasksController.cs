﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WorkersQualification.Models;
using System.Net.Mail;
using System.Threading.Tasks;
using Task = WorkersQualification.Models.Task;


namespace WorkersQualification.Controllers
{
    [Authorize]
    public class TasksController : Controller
    {
        private MartinEntities db = new MartinEntities();

        // GET: Tasks
        public ActionResult Index(string sortOrder,int AssignedId=0,int AssignerId=0, string searchString="")
        {

          
            

            var people = from p in db.Tasks
                         select p;

          

            ViewBag.AssignedId = new SelectList(db.People.AddRange(people.Select(x=>x.Person).Distinct()),"Id", "FullName");
            ViewBag.AssignerId = new SelectList(db.People.AddRange(people.Select(x=>x.Person1).Distinct()), "Id", "FullName");
            ViewBag.CurrentSort = sortOrder;
            ViewBag.DeadlineSortParm = sortOrder == "Deadline" ? "date_desc" : "Deadline";
            ViewBag.CompletedSortParm = sortOrder == "Completed" ? "date_desc2" : "Completed";
            ViewBag.AssignedIdSortParm = sortOrder == "AssignedId" ? "" : "AssignedId";

            if (AssignedId != 0) sortOrder = "AssignedId";
            if (AssignerId != 0) sortOrder = "AssignerId";
            {

            }

            switch (sortOrder)
            {

                case "Deadline":
                    people = people.OrderBy(p => p.Deadline);
                    return View(people
                .ToList());
                case "date_desc":
                    people = people.OrderByDescending(p => p.Deadline);
                    return View(people
                .ToList());
                case "Completed":
                    people = people.OrderByDescending(p => p.Completed);
                    return View(people
                 .ToList());
                case "date_desc2":
                    people = people.Where(x => x.Completed.HasValue).OrderBy(p => p.Completed);
                    return View(people
                .ToList());
                case "AssignedId":
                    people = people.Where(x => (AssignedId == x.AssignedId) || AssignedId == 0);
                    return View(people.ToList());
                case "AssignerId":
                    people = people.Where(x => (AssignerId == x.AssignerId) || AssignerId == 0);
                    return View(people.ToList());
                default:
                    break;
            }


            var tasks = db.Tasks.Include(t => t.Person).Include(t => t.Person1);

            if (!String.IsNullOrEmpty(searchString))
            {
                tasks = tasks.Where(t => t.TaskName.Contains(searchString));
            }

            return View(tasks.ToList());
        }

        // GET: Tasks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return View("~/Views/Home/Index.cshtml");
            }
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        // GET: Tasks/Create
        public ActionResult Create()
        {
            ViewBag.AssignedId = new SelectList(db.People, "Id", "FullName");
            ViewBag.AssignerId = new SelectList(db.People, "Id", "FullName");
            return View();
        }
        [CustomActionFilter]
        // POST: Tasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TaskName,Description,AssignerId,AssignedId,Deadline,State,Started,Completed")] Task task)
        {


            var description = task.Description;
            

            if (ModelState.IsValid)
            {
                db.Tasks.Add(task);
                db.SaveChanges();

                var id = task.AssignedId;
                var email = db.People.Where(x => x.Id == id).Single().Email;

                Email_send(description, email);
                return RedirectToAction("Index");
            }

            ViewBag.AssignedId = new SelectList(db.People, "Id", "FullName", task.AssignedId);
            ViewBag.AssignerId = new SelectList(db.People, "Id", "FullName", task.AssignerId);           
            return View(task);
        }

        // GET: Tasks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return View("~/Views/Home/Index.cshtml");
            }
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            ViewBag.AssignedId = new SelectList(db.People, "Id", "FullName", task.AssignedId);
            ViewBag.AssignerId = new SelectList(db.People, "Id", "FullName", task.AssignerId);
            return View(task);
        }
        [CustomActionFilter]
        // POST: Tasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TaskName,Description,AssignerId,AssignedId,Deadline,State,Started,Completed")] Task task)
        {
            if (ModelState.IsValid)
            {
                db.Entry(task).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AssignedId = new SelectList(db.People, "Id", "FullName", task.AssignedId);
            ViewBag.AssignerId = new SelectList(db.People, "Id", "FullName", task.AssignerId);
            return View(task);
        }

        // GET: Tasks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return View("~/Views/Home/Index.cshtml");
            }
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }
        [CustomActionFilter]
        // POST: Tasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Task task = db.Tasks.Find(id);
            db.Tasks.Remove(task);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public void Email_send(string description, string email)

             {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress("padevuspadevus@gmail.com");
            mail.To.Add(email);
            mail.Subject = "Sulle tuli uus ülesanne";
            mail.Body = $"Sulle tuli uus ülesanne: {description}. Saad seda vaadata siin: http://localhost:56955/People/Details/" + $"{Person.GetByEmail(email).Id}";

            //System.Net.Mail.Attachment attachment;
            ////attachment = new System.Net.Mail.Attachment("c:/textfile.txt");
            //mail.Attachments.Add(attachment);

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("padevuspadevus@gmail.com", "Pa$$w0rdPa$$w0rd");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);

        }
    }

}


