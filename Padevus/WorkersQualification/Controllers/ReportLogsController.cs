﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WorkersQualification.Models;

namespace WorkersQualification.Controllers
{
    [Authorize]
    public class ReportLogsController : Controller
    {
        private MartinEntities1 db = new MartinEntities1();

        // GET: ReportLogs
        public ActionResult Index(string searchString = "", int page = 0, int size = 25)
        {
            var reportLogs = from p in db.ReportLogs
                         select p;

            ViewBag.Page = page;
            ViewBag.Size = size;
            ViewBag.Pages = reportLogs.Count() / size;
            ViewBag.Count = reportLogs.Count();


            //for searching with multiple search words
            var searchWords = searchString.Split(' ');

            if (!String.IsNullOrEmpty(searchString))
            {
                foreach (var word in searchWords)
                {
                    reportLogs = reportLogs.Where(l => l.LogRecord.Contains(word));
                }                                         
            }

            return View(reportLogs
                .OrderBy(x => x.Id)
                .Skip(page * size)
                .Take(size)
                .ToList());
        }

        // GET: ReportLogs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReportLog reportLog = db.ReportLogs.Find(id);
            if (reportLog == null)
            {
                return HttpNotFound();
            }
            return View(reportLog);
        }

        // GET: ReportLogs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ReportLogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,LogDate,UserID,LogRecord")] ReportLog reportLog)
        {
            if (ModelState.IsValid)
            {              
                db.ReportLogs.Add(reportLog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(reportLog);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create2(ReportLog reportLog)
        {

            if (ModelState.IsValid)
            {
                db.ReportLogs.Add(reportLog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(reportLog);
        }
    }
}
