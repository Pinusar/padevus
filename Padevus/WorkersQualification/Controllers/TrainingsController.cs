﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WorkersQualification.Models;

namespace WorkersQualification.Controllers
{
    [Authorize]
    public class TrainingsController : Controller
    {
        private MartinEntities db = new MartinEntities();

        // GET: Trainings
        public ActionResult Index()
        {
            var trainings = db.Trainings.Include(t => t.Knowledge).Include(t => t.Level).Include(t => t.Person);
            return View(trainings.ToList());
        }

        // GET: Trainings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return View("~/Views/Home/Index.cshtml");
            }
            Training training = db.Trainings.Find(id);
            if (training == null)
            {
                return HttpNotFound();
            }
            return View(training);
        }

        // GET: Trainings/Create
        public ActionResult Create()
        {
            ViewBag.KnowId = new SelectList(db.Knowledges, "Id", "KnowNameEst");
            ViewBag.LeveId = new SelectList(db.Levels, "Id", "LevelNameEst");
            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName");
            return View();
        }
        [CustomActionFilter]
        // POST: Trainings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PersonId,TrainingName,TrainingPlace,KnowId,LeveId,TrainingDate,Completed")] Training training)
        {
            if (ModelState.IsValid)
            {
                db.Trainings.Add(training);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KnowId = new SelectList(db.Knowledges, "Id", "KnowNameEst", training.KnowId);
            ViewBag.LeveId = new SelectList(db.Levels, "Id", "LevelNameEst", training.LeveId);
            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName", training.PersonId);
            return View(training);
        }

        // GET: Trainings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return View("~/Views/Home/Index.cshtml");
            }
            Training training = db.Trainings.Find(id);
            if (training == null)
            {
                return HttpNotFound();
            }
            ViewBag.KnowId = new SelectList(db.Knowledges, "Id", "KnowNameEst", training.KnowId);
            ViewBag.LeveId = new SelectList(db.Levels, "Id", "LevelNameEst", training.LeveId);
            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName", training.PersonId);
            return View(training);
        }
        [CustomActionFilter]
        // POST: Trainings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PersonId,TrainingName,TrainingPlace,KnowId,LeveId,TrainingDate,Completed")] Training training)
        {
            if (ModelState.IsValid)
            {
                db.Entry(training).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KnowId = new SelectList(db.Knowledges, "Id", "KnowNameEst", training.KnowId);
            ViewBag.LeveId = new SelectList(db.Levels, "Id", "LevelNameEst", training.LeveId);
            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName", training.PersonId);
            return View(training);
        }

        // GET: Trainings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return View("~/Views/Home/Index.cshtml");
            }
            Training training = db.Trainings.Find(id);
            if (training == null)
            {
                return HttpNotFound();
            }
            return View(training);
        }
        [CustomActionFilter]
        // POST: Trainings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Training training = db.Trainings.Find(id);
            db.Trainings.Remove(training);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
