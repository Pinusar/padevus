Tiimi nimi ja liikmed: Dreamteam (Andres, Martin, Anti, Vladimir)

Mis ülesande valisite: Töötajate pädevuse hindamine

Kus repos te seda hoiate: https://gitlab.com/Pinusar/padevus.git


ülesande kirjeldus 

ülesande osad 

esimene sprint:
LUUA TÖÖTAJA KLASS
Propertyd: 
*nimi - TEHTUD
*isikukood - TEHTUD
*oskus ja tase - TEHTUD
*läbitud koolitused TEHTUD

teine sprint:
*tööülesanne
*tööülesande tähtaeg
*teavitus

Kolmas sprint:
*töötaja (mitte admini) vaade - osaliselt tehtud
*saab vaadata oma oskuseid ja ülesandeid

Neljas sprint:
*Sisselogimise vaade - TEHTUD

TÖÖTAJATEST LUUAKSE LIST, MILLE ABIL SAAB KUVADA

20.03
*Teha nii, et isikukoodi ei saaks muuta
*Sort ja filter
*Otsing (nime ja isikukoodi järgi)
*Tööülesanded

21.03
*nimekirja suurendamine 15-ni TEHTUD
*teha nii, et kuvataks 10 esimest TEHTUD
*pildid (piltide lisamine andmebaasi ja iga töötaja pildi lisamine) TEHTUD
*otsing isikukoodi järgi TEHTUD
*otsing märksõna järgi (java, excel) TEHTUD

*sünnipäevad

25.03 Tegemata asjad:
Funktsionaalsused: 
Töötajale peab olema vastavalt oskustele ja nende tasemele võimalik määrata sobivaid koolitusi ja eksameid 

Kui töötaja on lahkunud organisatsioonist, peab olema võimalik teda mitteaktiivseks määrata, see õigus on ainult admin-il 

Mitteaktiivsed töötajad ei tohi ilmuda tava otsingute tulemustesse 

Admin (nt raamatupidaja) saab otsida ka mitteaktiivseid kasutajaid 

Kui töötaja info on muudetud, peab töötaja infos kajastuma selle aeg ja muudatuse tegija 

Peab olema võimalik jälgida keskkonna tegevuste ajalugu 

Töötaja saab lisada enda koolituste ja eksamite tunnistusi ja diplomeid keskkonnas - TEHTUD

 



